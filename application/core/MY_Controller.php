<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller 
{ 
  //set the class variable.
  var $template  = array();
  var $data      = array();
  
  //Load layout    
  public function layout($layout = '') {
    if($layout == 'login'){
      $this->load->view('layout/login/index');
    }
    // making temlate and send data to view.
    $this->template['header']   = $this->load->view('layout/admin/header', $this->data, true);
    $this->template['left']   = $this->load->view('layout/admin/left', $this->data, true);
    $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
    $this->template['footer'] = $this->load->view('layout/admin/footer', $this->data, true);
    $this->load->view('layout/admin/index', $this->template);
  }

  public function accountlayout($accountlayout = '') {

    // making temlate and send data to view.
    $this->template['header']   = $this->load->view('layout/accountlayout/header', $this->data, true);
    $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
    $this->template['footer'] = $this->load->view('layout/pages/footer', $this->data, true);
    $this->template['ajax']   = $this->load->view('layout/pages/ajax', $this->data, true);

    $this->load->view('layout/accountlayout/index', $this->template);
  }

  public function accountlayout2($accountlayout2 = '') {

    // making temlate and send data to view.
    $this->template['header']   = $this->load->view('layout/accountlayout2/header', $this->data, true);
    $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
    $this->template['ajax']   = $this->load->view('layout/pages/ajax', $this->data, true);
    $this->template['footer'] = $this->load->view('layout/pages/footer', $this->data, true);
    $this->load->view('layout/accountlayout2/index', $this->template);
  }

  public function profilelayout($profilelayout = '') {

    // making temlate and send data to view.
    $this->template['header']   = $this->load->view('layout/accountlayout/header', $this->data, true);
    $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
    $this->template['ajax']   = $this->load->view('layout/pages/ajax', $this->data, true);
    $this->template['footer'] = $this->load->view('layout/profilelayout/footer', $this->data, true);
    $this->load->view('layout/profilelayout/index', $this->template);
  }


}