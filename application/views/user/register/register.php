<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="col-md-12">
			<div class="page-header">
				<h1>Register</h1>
			</div>
			<?= form_open() ?>
				<div class="col-md-6">
					<div class="form-group">
						<label>First Name</label>
						<input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter a firstname">
					</div>

					<div class="form-group">
						<label>Last Name</label>
						<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter a lastname">
					</div>

					<div class="form-group">
						<label>Email</label>
						<input type="text" class="form-control" id="email" name="email" placeholder="Enter a email">
					</div>

					<div class="form-group">
						<label>Mobile</label>
						<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter a mobile number">
					</div>

					<div class="form-group">
						<label>Landline</label>
						<input type="text" class="form-control" id="landline" name="landline" placeholder="Enter a landline number">
					</div>	

					<div class="form-group">
						<label for="password">Assessor Reg No</label>
						<input type="text" class="form-control" id="reg_no" name="reg_no">
					</div>	

					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Enter a password">
<!-- 					<p class="help-block">At least 6 characters</p>
 -->				</div>			
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Address Line 1</label>
						<input type="text" class="form-control" id="address1" name="address1" placeholder="Enter a address 1">
					</div>

					<div class="form-group">
						<label>Address Line 2</label>
						<input type="text" class="form-control" id="address2" name="address2" placeholder="Enter a address 2">
					</div>

					<div class="form-group">
						<label>City</label>
						<input type="text" class="form-control" id="city" name="city" placeholder="Enter city">
					</div>

					<div class="form-group">
						<label>Country</label>
						<input type="text" class="form-control" id="country" name="country" placeholder="Enter country">
					</div>

					<div class="form-group">
						<label>Post Code</label>
						<input type="text" class="form-control" id="post_code" name="post_code" placeholder="Enter a post code">
					</div>

					<div class="form-group">
						<label for="password">Lodgement Provider</label>
						<input type="text" class="form-control" id="lodgement_provider" name="lodgement_provider">
					</div>	

					<div class="form-group">
						<label for="password_confirm">Confirm password</label>
						<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm your password">
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12" style="margin-left:-30px">
							<label class="col-md-12" for="password_confirm">Services</label>
						</div>
						
						<div class="col-md-3">
							<input type="checkbox" name="service[domestic_epc]" value="domestic_epc"> <span>Domestic EPC</span>
						</div>

						<div class="col-md-3">
							<input type="checkbox" name="service[commercial_epc]" value="commercial_epc"> <span>Commercial EPC</span>
						</div>

						<div class="col-md-3">
							<input type="checkbox" name="service[floor_plan]" value="floor_plan"> <span>Floor Plan Sketch</span>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12" style="margin-left:-30px">
							<label class="col-md-12" for="password_confirm">Availability</label>
						</div>
					<table class="col-md-8">
						<tr> 
							<th class="col-md-2">&nbsp;</th>
							<th>Mon</th>
							<th>Tues</th>
							<th>Wed</th>
							<th>Thurs</th>
							<th>Fri</th>
							<th>Sat</th>
							<th>Sun</th>
							
						</tr>
						<tr>
							<td>0600-0900</td>
							<td><input type="checkbox" name="availability[6_9_mon]" value="6_9_mon"></td>
							<td><input type="checkbox" name="availability[6_9_tues]" value="6_9_tues"></td>
							<td><input type="checkbox" name="availability[6_9_wed]" value="6_9_wed"></td>
							<td><input type="checkbox" name="availability[6_9_thurs]" value="6_9_thurs"></td>
							<td><input type="checkbox" name="availability[6_9_fri]" value="6_9_fri"></td>
							<td><input type="checkbox" name="availability[6_9_sat]" value="6_9_sat"></td>
							<td><input type="checkbox" name="availability[6_9_sun]" value="6_9_sun"></td>
						</tr>
						<tr>
							<td>0900-1300</td>
							<td><input type="checkbox" name="availability[9_13_mon]" value="9_13_mon"></td>
							<td><input type="checkbox" name="availability[9_13_tues]" value="9_13_tues"></td>
							<td><input type="checkbox" name="availability[9_13_wed]" value="9_13_wed"></td>
							<td><input type="checkbox" name="availability[9_13_thurs]" value="9_13_thurs"></td>
							<td><input type="checkbox" name="availability[9_13_fri]" value="9_13_fri"></td>
							<td><input type="checkbox" name="availability[9_13_sat]" value="9_13_sat"></td>
							<td><input type="checkbox" name="availability[9_13_sun]" value="9_13_sun"></td>
						</tr>
						<tr>
							<td>1300-1800</td>
							<td><input type="checkbox" name="availability[13_18_mon]" value="13_18_mon"></td>
							<td><input type="checkbox" name="availability[13_18_tues]" value="13_18_tues"></td>
							<td><input type="checkbox" name="availability[13_18_wed]" value="13_18_wed"></td>
							<td><input type="checkbox" name="availability[13_18_thurs]" value="13_18_thurs"></td>
							<td><input type="checkbox" name="availability[13_18_fri]" value="13_18_fri"></td>
							<td><input type="checkbox" name="availability[13_18_sat]" value="13_18_sat"></td>
							<td><input type="checkbox" name="availability[13_18_sun]" value="13_18_sun"></td>
						</tr>
						<tr>
							<td>1800-2100</td>
							<td><input type="checkbox" name="availability[18_21_mon]" value="18_21_mon"></td>
							<td><input type="checkbox" name="availability[18_21_tues]" value="18_21_tues"></td>
							<td><input type="checkbox" name="availability[18_21_wed]" value="18_21_wed"></td>
							<td><input type="checkbox" name="availability[18_21_thurs]" value="18_21_thurs"></td>
							<td><input type="checkbox" name="availability[18_21_fri]" value="18_21_fri"></td>
							<td><input type="checkbox" name="availability[18_21_sat]" value="18_21_sat"></td>
							<td><input type="checkbox" name="availability[18_21_sun]" value="18_21_sun"></td>
						</tr>

					</table>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12" style="margin-left:-30px">
							<label class="col-md-12" for="password_confirm">Tell Us About You: (Message Box)</label>
						</div>
						
						<div class="col-md-12">
							<textarea name="message" class="col-md-12" rows="5" ></textarea>
						</div>
					</div>
				</div>
				

				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Register">
				</div>
			</form>
		</div>
	</div><!-- .row -->
</div><!-- .container -->