<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Home extends MX_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('supplier_model','supplier');
		
	}
	
	
	public function index() {
	
	$data['suppliers'] = $this->supplier->get_many_by(array('role' => 2));
	$this->load->view('header');

	$this->load->view('list_supplier',$data);
	$this->load->view('footer');

	}


	public function view($user_id) {

	$data['supplier'] = $this->supplier->get_by(array('id' => $user_id));
	$this->load->view('header');
	$this->load->view('view_supplier',$data);
	$this->load->view('footer');

	}



	
	/**
	 * logout function.
	 * 
	 * @access public
	 * @return void
	 */
	public function logout() {
		
		// create the data object
		$data = new stdClass();
		
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
			
			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}
			
			// user logout ok
			$this->load->view('header');
			$this->load->view('user/logout/logout_success', $data);
			$this->load->view('footer');
			
		} else {
			
			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/');
			
		}
		
	}
	
}
