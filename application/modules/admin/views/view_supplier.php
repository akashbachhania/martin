<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav navbar-left">
			<li><a class="btn btn-default" href="<?= base_url('admin/home') ?>">Back</a></li>
		</ul>
	</div>

	<div class="row">

		<div class="col-md-12">
			<div class="page-header">
				<h1>Supplier View</h1>
			</div>
			<table class="table table-striped">
				<tr>
					<th>First name</th>
					<td><?php echo $supplier->first_name?></td>
				</tr>
				<tr>	
					<th>Last name</th>
					<td><?php echo $supplier->last_name?></td>
				</tr>
				<tr>	

					<th>Email</th>
					<td><?php echo $supplier->email?></td>
				</tr>
				<tr>	
					<th>Mobile</th>
					<td><?php echo $supplier->mobile?></td>
				</tr>
				<tr>	
					<th>Landline</th>
					<td><?php echo $supplier->landline?></td>
				</tr>
				<tr>	
					<th>Reg no</th>
					<td><?php echo $supplier->reg_no?></td>
				</tr>
				<tr>	
					<th>Address1</th>
					<td><?php echo $supplier->address1?></td>
				</tr>
				<tr>	
					<th>Address2</th>
					<td><?php echo $supplier->address2?></td>
				</tr>
				<tr>	
					<th>Lodgement provider</th>
					<td><?php echo $supplier->lodgement_provider?></td>
				</tr>
				<tr>	
					<th>Address1</th>
					<td><?php echo $supplier->address1?></td>
				</tr>
				<tr>	
					<th>Address2</th>
					<td><?php echo $supplier->address2?></td>
				</tr>

				<tr>	
					<th>city</th>
					<td><?php echo $supplier->city?></td>
				</tr>
				<tr>	
					<th>country</th>
					<td><?php echo $supplier->country?></td>
				</tr>
				<tr>	
					<th>Post code</th>
					<td><?php echo $supplier->post_code?></td>
				</tr>
				<tr>	
					<th>service</th>
					<td><?php 
					$services = explode(',',$supplier->service);
					//echo '<pre>';print_r($services);die;
					foreach ($services as $key => $value) { 
						if(!empty($value)) {?>
						<div class="col-md-3">
							<input type="checkbox" checked="" disabled="" > <span><?php echo strtoupper(str_replace('_', "\t", $value));?></span>
						</div>
					
					<?php } } ?>
					</td>
				</tr>
				<tr>	
					<th>Lodgement Provider</th>
					<td><?php echo $supplier->lodgement_provider?></td>
				</tr>
				<tr>	
					<th>Availability</th>
					<td><?php 
						$i=$j=$k=$l=0;
					$availability = explode(',',$supplier->availability);
						//echo '<pre>';print_r($availability);die;
						foreach ($availability as $key => $value) { 
							if(!empty($value)) {
								$array = explode('_',$value);
								if(!empty($array[1]==9))
								{  if ($i==0){ echo '0600-0900'."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; $i++; } ?>
									<input type="checkbox" checked="" disabled="" > <span><?php echo ucfirst($array[2]);?></span>
								<?php }
								if(!empty($array[1]==13)) { if ($j==0){ echo '<br>'.'0900-1300'."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; $j++; } ?>
									<input type="checkbox" checked="" disabled="" > <span><?php echo ucfirst($array[2]);?></span>
								<?php }if(!empty($array[1]==18)) { if ($k==0){ echo '<br>'.'1300-1800'."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; $k++; } ?>
									<input type="checkbox" checked="" disabled="" > <span><?php echo ucfirst($array[2]);?></span>
								<?php } if(!empty($array[1]==21)) { if ($l==0){ echo '<br>'.'1800-2100'."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; $l++; } ?>
									<input type="checkbox" checked="" disabled="" > <span><?php echo ucfirst($array[2]);?></span>
							<?php } } } ?>
					</td>
				</tr>
				<tr>	
					<th>Message</th>
					<td><?php echo $supplier->message?></td>
				</tr>

			</table>

				
		</div>
	</div><!-- .row -->
</div><!-- .container -->