<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<div class="page-header">
				<h1>Supplier List</h1>
			</div>
			<table class="table table-striped">
				<tr>
					<th>First name</th>
					<th>Last name</th>
					<th>Email</th>
					<th>Mobile</th>
					<th>Landline</th>
					<th>Reg no</th>
					<th>Address1</th>
					<th>Lodgement provider</th>
					<th>View</th>

				</tr>
				<?php foreach ($suppliers as $key => $supplier) {

					?>
				<tr>
					<td><?php echo $supplier->first_name?></td>
					<td><?php echo $supplier->last_name?></td>
					<td><?php echo $supplier->email?></td>
					<td><?php echo $supplier->mobile?></td>
					<td><?php echo $supplier->landline?></td>
					<td><?php echo $supplier->reg_no?></td>
					<td><?php echo $supplier->address1?></td>
					<td><?php echo $supplier->lodgement_provider?></td>
					<td><a class="btn btn-default" href="<?php echo base_url("admin/home/view/".$supplier->id); ?>">view</a> </td>

				</tr>
				<?php } ?>

			</table>

				
		</div>
	</div><!-- .row -->
</div><!-- .container -->