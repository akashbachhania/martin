<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<div class="page-header">
				<h1>Profile</h1>
			</div>
			<table class="table table-striped">
				<tr>
					<th>First name</th>
					<td><?php echo $supplier->first_name?></td>
				</tr>
				<tr>	
					<th>Last name</th>
					<td><?php echo $supplier->last_name?></td>
				</tr>
				<tr>	

					<th>Email</th>
					<td><?php echo $supplier->email?></td>
				</tr>
				<tr>	
					<th>Mobile</th>
					<td><?php echo $supplier->mobile?></td>
				</tr>
				<tr>	
					<th>Landline</th>
					<td><?php echo $supplier->landline?></td>
				</tr>
				<tr>	
					<th>Reg no</th>
					<td><?php echo $supplier->reg_no?></td>
				</tr>
				<tr>	
					<th>Address1</th>
					<td><?php echo $supplier->address1?></td>
				</tr>
				<tr>	
					<th>Lodgement provider</th>
					<td><?php echo $supplier->lodgement_provider?></td>
				</tr>

			</table>

				
		</div>
	</div><!-- .row -->
</div><!-- .container -->