<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Home extends MX_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('supplier_model','supplier');


		
	}
	
	
	public function index() {
	$data['supplier'] = $this->supplier->get_by(array('id' => $this->session->user_id));
	$this->load->view('header');
	$this->load->view('supplier',$data);
	$this->load->view('footer');
	}

}
